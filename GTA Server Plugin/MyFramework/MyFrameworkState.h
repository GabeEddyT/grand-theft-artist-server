#ifndef _MYFRAMEWORKSTATE_H
#define _MYFRAMEWORKSTATE_H

#include "RakNet/RakPeerInterface.h"
#include "RakNet/MessageIdentifiers.h"
#include "RakNet/RakNetTypes.h"  
#include <string>
#include "EventManager.h"
struct InputMessage
{
	byte id = 0;
	float vertical = 0.0f;
	float horizontal = 0.0f;
};



class MyFrameworkState
{

	RakNet::RakPeerInterface *peer;
	std::string address;
public:
	~MyFrameworkState();
	void init(int serverPort = 60);
	char * getPacket();
	RakNet::Packet* getRakPacket();

	inline RakNet::RakNetGUID getGUID() { return peer->GetMyGUID(); }
	void sendPacket(char* packet, int size = 8, RakNet::AddressOrGUID id = RakNet::UNASSIGNED_SYSTEM_ADDRESS);
	void broadcastPacket(char* packet, int size = 8);
	int Networking();
	int StateFoo(int bar);
	const char * getIP();
	EventManager *mpEventQueue;
	//static MyFrameworkState *gpState;
	//inline MyFrameworkState() { gpState = this; };
};
 

#endif // !_MYFRAMEWORKSTATE_H

