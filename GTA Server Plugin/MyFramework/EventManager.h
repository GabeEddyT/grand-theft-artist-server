#pragma once
#include "Event.h"
#include "Queue.h"
class EventManager
{
private:
	Queue<Event*> *mpEventQueue;
public:
	void addEvent(Event *eventToAdd);
	void executeEvents();
};