#include "EventManager.h"

void EventManager::addEvent(Event *eventToAdd)
{
	mpEventQueue->enqueue(eventToAdd);
}
void EventManager::executeEvents()
{
	while (!mpEventQueue->isEmpty())
	{
		Event* temp;
		temp = mpEventQueue->dequeue();
		temp->execute();
	}
} 