//EGP 405-02 Network Programming for Games 
//Certificate of Authenticity:
//�We certify that this work is entirely our own.Jack Malvey (1005854) and Gabe Troyan (1021105)
//The assessor of this project may reproduce this project
//and provide copies to other academic staff, and/or communicate a copy of
//this project to a plagiarism - checking service, which may retain a copy of the project on its database.�
#include "MyFrameworkState.h"
#include <string.h> // MessageID
#include <fstream>
#include <sstream>
#include <string>
#include <io.h>
#include <iostream>
#include <fcntl.h>
#include <tchar.h>


int MyFrameworkState::StateFoo(int bar)
{
	return (bar * bar);
}

const char * MyFrameworkState::getIP()
{
	return address.c_str();
}

MyFrameworkState::~MyFrameworkState()
{
	peer->Shutdown(0);
	RakNet::RakPeerInterface::DestroyInstance(peer);
}

void MyFrameworkState::init(int serverPort)
{
	
	peer = RakNet::RakPeerInterface::GetInstance();
	RakNet::SocketDescriptor sd(serverPort, 0);

	peer->Startup(100, &sd, 1);
	address = peer->GetLocalIP(2);
	//serverPort = atoi(str);

	// TODO - Add code body here


	//fgets(str, 512, stdin);

	peer->SetMaximumIncomingConnections(100);// 100 for debugging purposes

	//peer->Connect(ip, serverPort, 0, 0);

	peer->SetTimeoutTime(300000000, RakNet::UNASSIGNED_SYSTEM_ADDRESS);

}
//give more time to avoid the timeout while debugging



char* MyFrameworkState::getPacket()
{
	
	static RakNet::Packet *packet;
	peer->DeallocatePacket(packet);
	//int index = guid.;
	packet = peer->Receive();
	if (packet)
	{		
		return (char*)packet->data;
	}
	else
	{
		

		return NULL;
	}
	switch (packet->data[0])
	{
	case ID_REMOTE_DISCONNECTION_NOTIFICATION:
		wprintf(L"Another client has disconnected.\n");
		break;
	case ID_REMOTE_CONNECTION_LOST:
		wprintf(L"Another client has lost the connection.\n");
		break;
	case ID_REMOTE_NEW_INCOMING_CONNECTION:
		wprintf(L"Another client has connected.\n");
		break;
	case ID_NO_FREE_INCOMING_CONNECTIONS:
		wprintf(L"The server is full.\n");
		break;
	case ID_CONNECTION_REQUEST_ACCEPTED:
	{
		system("cls");
	}
	break;
	case ID_NEW_INCOMING_CONNECTION:
	{
		system("cls");
	}
	break;
	case ID_DISCONNECTION_NOTIFICATION:
	{
		wprintf(L"We have been disconnected.\n");

	}
	break;
	case ID_CONNECTION_LOST:
		wprintf(L"Connection lost.\n");
		break;
		

	default:
		wprintf(L"Message with identifier %i has arrived.\n", packet->data[0]);
		break;
	}
	peer->DeallocatePacket(packet);
	

}

RakNet::Packet* MyFrameworkState::getRakPacket()
{
	static RakNet::Packet *prev;
	peer->DeallocatePacket(prev);
	prev = peer->Receive();
	if (prev)
	{
		return prev;
	}
	else
	{
		return NULL;
	}
}

void MyFrameworkState::sendPacket(char * packet, int size, RakNet::AddressOrGUID id)
{
	
	peer->Send(packet, size, HIGH_PRIORITY, RELIABLE_ORDERED, 0, id, false);
}

void MyFrameworkState::broadcastPacket(char * packet, int size)
{
	peer->Send(packet, size, HIGH_PRIORITY, RELIABLE_ORDERED, 0, peer->GetMyGUID(), true);
}

