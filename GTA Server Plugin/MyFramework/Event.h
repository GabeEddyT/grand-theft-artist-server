#pragma once
#include <tchar.h>

enum EventType 
{
	INVALID__EVENT = -1,
	COLLISION_EVENT,
	DETECT_EVENT,
	STEAL_EVENT,
	MONEY_EVENT,
	MOVE_EVENT
}; 
struct EventTypeStruct
{
	unsigned char ID = 138;
	int eventID;
};
class Event
{
public:
	Event();
	Event(EventType type);
	Event(int type);
	~Event();

	virtual void execute();
	EventType getType();
private:
	EventType mType;
};