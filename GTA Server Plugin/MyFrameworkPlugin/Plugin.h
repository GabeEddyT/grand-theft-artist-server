#ifndef _PLUGIN_H
#define _PLUGIN_H



#include "Lib.h"

#ifdef __cplusplus
extern "C"
{
#endif // __cplusplus

	typedef struct SmartPacket
	{
		unsigned char id = 0;
		unsigned char* data = 0;
		unsigned long long guid = 0;
		char sysAddress[32];
	}SmartPacket, *PSmartPacket;

	//	startup and shutdown
	MYPLUGIN_SYMBOL int Startup();
	MYPLUGIN_SYMBOL int Shutdown();

	//	c-style function to be called
	MYPLUGIN_SYMBOL int Foo(int bar);
	MYPLUGIN_SYMBOL char* CStringTest();
	MYPLUGIN_SYMBOL const char* getGUID();

	MYPLUGIN_SYMBOL char* InputTest(char* stuff);
	MYPLUGIN_SYMBOL char* getTest();
	MYPLUGIN_SYMBOL char* returnToSender(char* delivery);
	MYPLUGIN_SYMBOL int initNetworking(int serverPort = 60);
	MYPLUGIN_SYMBOL char* getNetworkPacket();
	MYPLUGIN_SYMBOL struct SmartPacket getSmartPacket();
	MYPLUGIN_SYMBOL void sendChatMessage(char* message);
	MYPLUGIN_SYMBOL void sendNetworkPacket(char* packet, int size = 8, unsigned long long destID = 0);
	MYPLUGIN_SYMBOL void broadcastNetworkPacket(char* packet, int size = 8);
	MYPLUGIN_SYMBOL const char* getIP();
	MYPLUGIN_SYMBOL void addingEvent(int type);
	MYPLUGIN_SYMBOL void execution();
#ifdef __cplusplus
}
#endif // __cplusplus


#endif // !_PLUGIN_H
