﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.InteropServices;

public class ItemSpawn : MonoBehaviour {
    //[DllImport("MyFrameworkPlugin")]
    //static extern void addingEvent(int type);

    Item[] items;
    
    int randomNum;
    Onomatopoeia ono;
    
	// Use this for initialization
	void Start ()
    {
        items = Resources.LoadAll<Item>("Prefabs/Items");
        ono = Resources.LoadAll<Onomatopoeia>("Prefabs")[0];
    }
	
	// Update is called once per frame
	void Update ()
    {

    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        StartCoroutine(Spawn(collision));
    }

    IEnumerator Spawn(Collision2D collision)
    {
        
        //addingEvent((int)NetworkInput.EventType.COLLISION_EVENT);
        Vector3 pos = collision.transform.position;
        //Debug.Log(collision.relativeVelocity.magnitude);
        ContactPoint2D[] contacts = new ContactPoint2D[1];

        int numContacts = collision.GetContacts(contacts);

        if (numContacts < 1)
        {
            yield break;
        }
        
        Vector2 vec = collision.rigidbody.GetPointVelocity(contacts[0].point);

        if (collision.rigidbody && !collision.rigidbody.isKinematic && (vec.magnitude > 20) && collision.gameObject.layer != 12)
        {
            // Vector2 vec = collision.contacts[0].point - new Vector2(gameObject.transform.position.x, gameObject.transform.position.y);
            //Vector2 vec = collision.relativeVelocity;
            //Debug.Log( collision.rigidbody.GetPointVelocity(collision.contacts[0].point) + " " +vec);
            //vec.Normalize();
            //vec.Scale(new Vector2(-1, -1));
            // vec.Scale(new Vector2(-10, -10));

            

            Onomatopoeia newOno = Instantiate(ono);
            newOno.GetComponent<AudioSource>().volume = vec.magnitude / 100;
            newOno.scale = vec.magnitude / 10;
            newOno.transform.localPosition = (Vector3)contacts[0].point + new Vector3(0, 0, -5);

            int itemsToSpawn = 0;
            int val = (int)vec.magnitude;

            if (val > 70)
            {
                itemsToSpawn = 13;
            }
            else if (val > 60)
            {
                itemsToSpawn = 8;
            }
            else if (val > 50)
            {
                itemsToSpawn = 5;
            }
            else if (val > 40)
            {
                itemsToSpawn = 3;
            }
            else if (val > 30)
            {
                itemsToSpawn = 2;
            }
            else
                itemsToSpawn = 1;

            byte[] itemType = new byte[itemsToSpawn];
            Vector2[] trajectory = new Vector2[itemsToSpawn];
            float[] rotVelocity = new float[itemsToSpawn];

            for (int i = 0; i < itemsToSpawn; i ++) 
			{
				randomNum = Random.Range(0, items.Length);
                
				Item item = Instantiate(items[randomNum], contacts[0].point + vec.normalized * 2, transform.rotation);
				
				item.gameObject.layer = 12;

				CircleCollider2D myCollider = item.gameObject.AddComponent<CircleCollider2D>();
				GameObject child = new GameObject();
				child.transform.parent = item.transform;
				child.layer = 12;
				child.transform.localPosition = Vector2.zero;
				
				CircleCollider2D childCol = child.gameObject.AddComponent<CircleCollider2D>();
				childCol.radius = 1;
				

				myCollider.isTrigger = true;
				item.gameObject.AddComponent<Rigidbody2D>();
				Rigidbody2D rb = item.gameObject.GetComponent<Rigidbody2D>();
				

				rb.mass = .5f;
				rb.drag = 1f;
				rb.angularDrag = 1f;
                float randTorque = Random.value * 50f - 25f;
				rb.AddTorque(randTorque);

				float randomAngle = Random.value * 180 - 90;
				Matrix4x4 rotMat = Matrix4x4.TRS(Vector3.zero, Quaternion.Euler(0, 0, randomAngle), Vector3.one);
				
				rb.velocity = rotMat.MultiplyVector(vec);

                // Shit to send to NetworkInput
                itemType[i] = (byte)randomNum;
                trajectory[i] = rb.velocity;
                rotVelocity[i] = randTorque;

                yield return null;
			}

            FindObjectOfType<NetworkInput>().SendItems((byte)itemsToSpawn, contacts[0].point + vec.normalized * 2, itemType, trajectory, rotVelocity);
        }
    }
}
