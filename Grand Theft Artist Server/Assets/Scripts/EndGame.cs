﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EndGame : MonoBehaviour {

    public Player player;
    BoxCollider2D box;
	// Use this for initialization
	void Start () {
        box = GetComponent<BoxCollider2D>();
	}
	
	// Update is called once per frame
	void Update ()
    {
        checkWinstate();
	}

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.name == player.name)
        {
            FindObjectOfType<NetworkInput>().Win(player);
            
            SceneManager.LoadScene("Success");
        }
    }

    private void checkWinstate()
    {
        if (player.cash >= player.goalAmount)
        {
            box.enabled = true;
        }
        else
            box.enabled = false;
    }
}
