﻿using System.Collections;
using System.Collections.Generic;
using System;
using System.Text;
using UnityEngine;
using UnityEngine.UI;
using System.Runtime.InteropServices;
using System.IO;
using System.Net;
using System.Security.Cryptography.X509Certificates;
using System.Net.Security;

public class NetworkInput : MonoBehaviour
{ 

    public Player[] players;

    [DllImport("MyFrameworkPlugin")]
    static extern int Startup();

    [DllImport("MyFrameworkPlugin")]
    static extern int Shutdown();

    [DllImport("MyFrameworkPlugin")]
    static extern unsafe char* CStringTest();

    [DllImport("MyFrameworkPlugin")]
    static extern IntPtr InputTest(IntPtr stuff);

    [DllImport("MyFrameworkPlugin")]
    static extern IntPtr getTest();
    [DllImport("MyFrameworkPlugin")]
    static extern IntPtr returnToSender(IntPtr delivery);
    [DllImport("MyFrameworkPlugin")]
    static extern int initNetworking(int serverPort = 60);
    [DllImport("MyFrameworkPlugin")]
    static extern unsafe char* getIP();
    [DllImport("MyFrameworkPlugin")]
    static extern unsafe char* getNetworkPacket();
    [DllImport("MyFrameworkPlugin")]
    static extern void sendNetworkPacket(IntPtr packet, int size = 8, UInt64 destID = 0);
    [DllImport("MyFrameworkPlugin")]
    static extern unsafe void sendChatMessage(string message);
    [DllImport("MyFrameworkPlugin")]
    static extern IntPtr getGUID();
    [DllImport("MyFrameworkPlugin")]
    static extern void broadcastNetworkPacket(IntPtr packet, int size = 8);
    [DllImport("MyFrameworkPlugin")]
    static extern SmartPacket getSmartPacket();

    int numPlayers = 0;
    Dictionary<UInt64, int> guidMap;

    public Text ipAddress;
    private ItemSpawn[] shelves;
    
    
    bool initFlag = false;


    enum Messages
    {
        INCOMING = 19,
        CONNECTION_LOST = 22,
        CLIENT_DISCONNECT = 31,
        CLIENT_LOST,
        MESSAGE = 135,
        INPUT = 136,
        GUID,
        EVENT,
        GAMESTATE,
        ITEMSPAWN,
        SHELF,
        WINNER
    }

    public enum EventType
    {
        INVALID__EVENT = -1,
        COLLISION_EVENT,
        DETECT_EVENT,
        STEAL_EVENT,
        MONEY_EVENT,
        MOVE_EVENT
    };

    [StructLayout(LayoutKind.Sequential)]
    public struct SmartPacket
    {
        public byte id;
        public IntPtr data;
        public UInt64 guid;
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 32)]
        public byte[] sysAddress;
    };



    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public unsafe struct GameState
    {
        public byte id;
        public fixed float playerPosX[4];
        public fixed float playerPosY[4];
        public fixed float playerRotation[4];
        public fixed ulong playerGuid[4];
        public fixed float playerCash[4];
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 4)]
        public Vector2[] playerVelocity;
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 4)]
        public Vector2[] playerAxes;
        public double timestamp;
    };

    /**
     * Struct to send the state of the shelves for a synchronized experience.
     * */
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public unsafe struct ShelfState
    {
        public byte id;

        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 14)]
        public BetaString[] name;
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 14)]
        public Vector2[] shelfPos;
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 14)]
        public Quaternion[] shelfRot;
    };

    /**
        Struct to send the ItemSpawn data in hopes of recreating them on the client end.
     */
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public unsafe struct ItemShip
    {
        public byte id;
        public byte numItems;
        public Vector2 location;
        public fixed byte itemType[13];
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 13)]
        public Vector2[] trajectory;
        public fixed float rotVelocity[13];
    };


    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public unsafe struct InputMessage
    {
        public byte id;
        public float vertical;
        public float horizontal;

    }


    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public unsafe struct BetaString
    {
        public byte id;
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 256)] // thank you, Stack: https://stackoverflow.com/a/24539071
        public byte[] pseudoString;
    }

    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct WinMessage
    {
        public byte id;
        public UInt64 winner;
    }

    private void Awake()
    {
        players = FindObjectsOfType<Player>();
        shelves = FindObjectsOfType<ItemSpawn>();
    }

    // Use this for initialization
    unsafe void Start()
    {
        Debug.Log("Size of data: " + Marshal.SizeOf(typeof(GameState)));
        Debug.Log("Shutdown: " + Shutdown());
        Debug.Log("Startup: " + Startup());
        FileStream file = new FileStream(Application.streamingAssetsPath + "/server-port.txt", FileMode.Open, FileAccess.Read);
        
        StreamReader sr = new StreamReader(file);
        String serverPort = sr.ReadLine();
        if (serverPort != null)
        {
            initNetworking(Int32.Parse(serverPort));
        }

        guidMap = new Dictionary<ulong, int>();
        initFlag = true;
        
        WebClient webClient = new WebClient();
        ipAddress.text = webClient.DownloadString("http://api.ipify.org") + "│" + serverPort;
        Debug.Log("Starting the server @ " + ipAddress.text);

        StartCoroutine(ManageShelf());
        StartCoroutine(ManageState());
    }

    // Update is called once per frame
    void Update()
    {
        if (initFlag)
        {
            ProcessSmartPacket();
        }
    }

    private void OnDestroy()
    {
        Shutdown();
    }

    IEnumerator ManageState()
    {
        while (true)
        {
            SendGameState();
            yield return new WaitForSeconds(.033f);
        }
    }

    IEnumerator ManageShelf()
    {
        while (true)
        {
            SendShelfState();
            yield return new WaitForSeconds(.33f);
        }
    }

    unsafe void SendGameState()
    {
        GameState stateToSend = new GameState();
        stateToSend.playerVelocity = new Vector2[4];
        stateToSend.playerAxes = new Vector2[4];

        //copying guids from the map to an array and hoping in the name of holy connections that arrays won't break with uninitialised values
        foreach (var item in guidMap)
        {
            stateToSend.playerGuid[item.Value] = item.Key;
        }
        TimeSpan t = DateTime.UtcNow - DateTime.MinValue; //consistent epoch time

        stateToSend.timestamp = t.TotalSeconds;

        for (int i = 0; i < 4; i++)
        {
            stateToSend.playerPosX[i] = players[i].transform.position.x;
            stateToSend.playerPosY[i] = players[i].transform.position.y;
            stateToSend.playerRotation[i] = players[i].transform.rotation.z;
            stateToSend.playerVelocity[i] = players[i].GetComponent<Rigidbody2D>().velocity;
            stateToSend.playerAxes[i] = new Vector2(players[i].horAxis, players[i].speed); //send all input         
            stateToSend.playerCash[i] = players[i].cash; // setting up PayPal
        }

        stateToSend.id = (byte)Messages.GAMESTATE;

        int size = Marshal.SizeOf(typeof(GameState));

        IntPtr myPtr = Marshal.AllocHGlobal(size);
        Marshal.StructureToPtr(stateToSend, myPtr, false);

        ForwardMessageToAll(myPtr, typeof(GameState));

        Marshal.FreeHGlobal(myPtr);
    }

    /**
     * Send the date of the shelves using arrays of their names, positions, and rotations.
     * */
    unsafe void SendShelfState()
    {
        ShelfState myShelves;
        myShelves.id = (byte)Messages.SHELF;
        myShelves.name = new BetaString[14];
        myShelves.shelfPos = new Vector2[14];
        myShelves.shelfRot = new Quaternion[14];

        for (byte i = 0; i < shelves.Length; i++)
        {
            myShelves.name[i].pseudoString = ToByte(shelves[i].name);
            myShelves.shelfPos[i] = shelves[i].transform.position;
            myShelves.shelfRot[i] = shelves[i].transform.rotation;
        }

        ForwardMessageToAll(myShelves);
    }


    /**
        Method to be called from ItemSpawn; Packs provided params into the ItemShip struct
     */
    public unsafe void SendItems(byte numItems, Vector2 location, byte[] itemType, Vector2[] trajectory, float[] rotVelocity)
    {
        ItemShip shipment;
        shipment.id = (byte)Messages.ITEMSPAWN;
        shipment.numItems = numItems;
        shipment.location = location;
        shipment.trajectory = new Vector2[numItems];

        for (byte i = 0; i < numItems; ++i)
        {
            shipment.itemType[i] = itemType[i];
            shipment.trajectory[i] = trajectory[i];
            shipment.rotVelocity[i] = rotVelocity[i];
        }

        ForwardMessageToAll(shipment);
    }

    void ProcessSmartPacket()
    {
        SmartPacket packet = getSmartPacket();
        if (packet.data == IntPtr.Zero)
        {
            return;
        }
        switch (packet.id)
        {
            case (byte)Messages.INCOMING:
                Debug.Log("Client connected: " + FromByte(packet.sysAddress));

                guidMap.Add(packet.guid, guidMap.Count);
                numPlayers++;
                break;
            case (byte)Messages.CLIENT_DISCONNECT:
                Debug.Log("Client disconnected: " + FromByte(packet.sysAddress));
                guidMap.Remove(packet.guid);
                numPlayers--;
                break;
            case (byte)Messages.MESSAGE:
                ForwardMessageToAll(packet.data, typeof(BetaString));
                break;
            case (byte)Messages.INPUT:
                {
                    HandleInput(packet);
                }
                break;
            default:
                Debug.Log("Packet with unknown id, " + packet.id + ", received from " + FromByte(packet.sysAddress));
                break;
        }
    }


    unsafe void HandleInput(SmartPacket packet)
    {
        InputMessage im = (InputMessage)Marshal.PtrToStructure(packet.data, typeof(InputMessage));

        players[guidMap[packet.guid]].UpdateAxes(im.horizontal, im.vertical);

    }

    /* *
    * Generic method to abstract the conversion to IntPtr when sending packet. 
    * */
    void SendPkt<T>(T pkt)
    {
        int size = Marshal.SizeOf(pkt);
        IntPtr myPtr = Marshal.AllocHGlobal(size);
        Marshal.StructureToPtr(pkt, myPtr, false);
        sendNetworkPacket(myPtr, size);
        Marshal.FreeHGlobal(myPtr);
    }

    /* *
    * Abstract the conversion of strings to byte arrays. 
    * */
    byte[] ToByte(string s)
    {
        return Encoding.ASCII.GetBytes(s);
    }

    /* *
     *  Abstract getting strings from byte arrays
     * */
    string FromByte(byte[] b)
    {
        return Encoding.ASCII.GetString(b);
    }

    unsafe void ForwardMessageToAll(IntPtr packet, Type t)
    {
        broadcastNetworkPacket(packet, Marshal.SizeOf(t));
    }

    public void Win(Player winner)
    {
        foreach (var aGuid in guidMap)
        {
            if (winner.name == players[aGuid.Value].name)
            {
                WinMessage wm;
                wm.id = (byte)Messages.WINNER;
                wm.winner = aGuid.Key;
                ForwardMessageToAll(wm);
            }
        }
    }

    void ForwardMessageToAll<T>(T pkt)
    {
        int size = Marshal.SizeOf(pkt);
        IntPtr myPtr = Marshal.AllocHGlobal(size);
        Marshal.StructureToPtr(pkt, myPtr, false);
        broadcastNetworkPacket(myPtr, size);
        Marshal.FreeHGlobal(myPtr);
    }
}
