﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEditor.Callbacks;
using System.IO;
using System;

public class BatBuilder : MonoBehaviour
{
    [PostProcessBuildAttribute(1)]
    public static void OnPostprocessBuild(BuildTarget target, string pathToBuiltProject)
    {
#if UNITY_STANDALONE_WIN
        BuildBatch(pathToBuiltProject);
#endif
#if UNITY_STANDALONE_LINUX || UNITY_STANDALONE_OSX
        BuildBash(pathToBuiltProject);
#endif
    }

    static void BuildBatch(string pathToBuiltProject)
    {
        string path = Path.GetDirectoryName(pathToBuiltProject);
        string serverPortFile = Path.GetFileNameWithoutExtension(pathToBuiltProject) + "_Data\\StreamingAssets\\server-port.txt";
        FileStream file = new FileStream(path + "\\set server port.bat", FileMode.Create, FileAccess.ReadWrite);
        StreamWriter sr = new StreamWriter(file);
        sr.WriteLine("@echo off" +
        "\nset /p serverPort=<\"{0}\"" +
        "\n:start" +
        "\necho Current server port is: %serverPort%" +
        "\nset /p newPort=New port: " +
        "\nif not defined newPort exit" +
        "\necho %newPort% | findstr /r \"[0-9]\" > nul" +
        "\nif errorlevel 1 goto err" +
        "\nif %newPort% gtr 65535 goto err" +
        "\nif %newPort% lss 1 goto err" +
        "\necho %newPort%>\"{0}\"" +
        "\nexit" +
        "\n:err" +
        "\necho \"%newPort%\" is invalid; Enter a number between 1 and 65535." +
        "\nset \"newPort=\"" +
        "\ngoto start", serverPortFile);
        sr.Close();
    }

    static void BuildBash(string pathToBuiltProject)
    {
        string path = Path.GetDirectoryName(pathToBuiltProject);
        string serverPortFile = Path.GetFileNameWithoutExtension(pathToBuiltProject) + "_Data/StreamingAssets/server-port.txt";
        FileStream file = new FileStream(path + "/set-server-port", FileMode.Create, FileAccess.ReadWrite);
        StreamWriter sr = new StreamWriter(file);
        sr.WriteLine("#!/bin/bash" +
        "\nerr()" +
        "\n{" +
        "\n    echo \"\\\"$newPort\\\" is invalid; Enter a number between 1 and 65535.\"" +
        "\n    newPort=" +
        "\n}");
        sr.WriteLine("\nserverPort=$(cat \"{0}\")", serverPortFile);
        sr.WriteLine("\nwhile true; do"+
        "\n    echo Current server port is: $serverPort"+
        "\n    echo -n 'New port: '"+
        "\n    read newPort"+
        "\n    if [ -z $newPort ]; then"+
        "\n        exit 1"+
        "\n    fi"+
        "\n    echo $newPort | grep [0-9] > /dev/null"+
        "\n    if [ $? -eq 1 ] || [ $newPort -gt 65535 ] || [ $newPort -lt 1 ]; then"+
        "\n        err"+
        "\n        continue"+
        "\n    fi"+
        "\n    echo $newPort > \"{0}\""+
        "\n    break"+
        "\ndone"+
        "\nexit 0", serverPortFile);
        sr.Close();
    }
}
